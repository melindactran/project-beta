import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileList from './inventory/AutomobileList';
import AutomobileForm from './inventory/AutomobileForm';
import ManufacturersList from './ManufacturersList';
import ManufacturersForm from './ManufacturersForm';
import ModelsList from './ModelsList';
import ModelsForm from './ModelsForm';
import TechnicianList from './service/TechnicianList';
import CreateTechnician from './service/TechnicianForm';
import AppointmentList from './service/AppointmentList';
import CreateAppointment from './service/AppointmentForm';
import ServiceHistory from './service/ServiceHistory';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="inventory">
            <Route index element={<AutomobileList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>

          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="create" element={<ManufacturersForm />} />
          </Route>

          <Route path="models">
            <Route index element={<ModelsList />} />
            <Route path="create" element={<ModelsForm />} />
          </Route>
{/*
          <Route path="salespeople">
            <Route index element={<SalesPeopleList />} />
            <Route path="create" element={<SalespeopleForm />} />
          </Route>

          <Route path="customer">
            <Route index element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>

          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route> */}

          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="create" element={<CreateTechnician />} />
          </Route>

          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="create" element={<CreateAppointment />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
