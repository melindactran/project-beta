import React, { useEffect, useState } from 'react';

export default function ModelsList() {
    const [models, setModels] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        const data = await response.json();
        setModels(data.models)
    }
    useEffect(() => {
        getData()
    }, [])
    return (
        <div>
            <h1>List of Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(models => {
                        return (
                            <tr key={models.id}>
                                <td>{models.name}</td>
                                <td>{models.manufacturer.name}</td>
                                <td><img src={models.picture_url}
                                width="520"
                                height="300" /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
