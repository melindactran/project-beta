import React, { useEffect, useState } from 'react';

export default function AppointmentList() {
    const[appointments, setAppointments] = useState([]);

    const loadData = async () => {
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url);
        const apptData = await response.json();
        setAppointments(apptData.appointments)
    }

    const finishedAppt = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: 'put',
        });
    }

    const canceledAppt = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: 'put',
        });
    }

    useEffect(() => {
        loadData()
    }, [])


    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP?</th>
                        <th>Customer</th>
                        <th>Date & Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{String(appointment.vip)}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date_time}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td><button type="button"
                                    className='btn btn-success options-outlined'
                                    onClick={() => finishedAppt(appointment.id)}>Finish</button></td>
                                <td><button type="button"
                                    className='btn btn-danger options-outlined'
                                    onClick={() => canceledAppt(appointment.id)}>Cancel</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
