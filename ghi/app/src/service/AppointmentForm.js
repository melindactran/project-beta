import React, { useEffect, useState } from 'react';

export default function AppointmentForm() {
  const [dateTime, setDateTime] = useState('');
  const [reason, setReason] = useState('');
  const [status, setStatus] = useState('');
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [technician, setTechnician] = useState('');
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    async function loadTech() {
      const response = await fetch('http://localhost:8080/api/technicians/');
      if (response.ok) {
        const techData = await response.json();
        setTechnicians(techData.technicians);
      }
    }
    loadTech();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      date_time: dateTime,
      reason,
      status,
      vin,
      customer,
      technician,
    };

    const url = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setDateTime('');
      setReason('');
      setStatus('');
      setVin('');
      setCustomer('');
      setTechnician('');
    }
  };

  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  };
  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };
  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };
  const handleTechChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create A Service Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="first_name" id="first_name" className="form-control"/>
              <label htmlFor="first_name">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleDateTimeChange} value={dateTime} placeholder="Date and Time" required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
              <label htmlFor="date_time">Date and Time</label>
            </div>
            <div className="mb-3">
              <select required onChange={handleTechChange} value={technician} name="technician" id="technician" className="form-select">
                <option value="">Choose a technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.first_name} {technician.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
            <label htmlFor="reason">Reason</label>
              <textarea onChange={handleReasonChange} value={reason} id="reason" rows="3" name="reason" className="form-control"></textarea>
            </div>
            <button className="btn btn-primary">Create Appointment</button>
          </form>
        </div>
      </div>
    </div>
  );
}
