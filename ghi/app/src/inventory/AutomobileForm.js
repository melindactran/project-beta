import React, { useState, useEffect } from 'react';

export default function AutomobileForm() {
    const [vin, setVin] = useState('');
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [model_id, setModel] = useState('');
    const [models, setModels] = useState([]);

    useEffect(() => {
        const loadAutomobileData = async () => {
            const automobileResponse = await fetch('http://localhost:8100/api/models/');
            const automobileData = await automobileResponse.json();
            setModels(automobileData.models);
        };
        loadAutomobileData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            vin,
            color,
            year,
            model_id,
        };
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(automobileUrl, fetchConfig);

        if (response.ok) {
            setVin('');
            setColor('');
            setYear('');
            setModel('');
        }
    };

    const handleVinChange = (event) => {
        setVin(event.target.value);
    };

    const handleColorChange = (event) => {
        setColor(event.target.value);
    };

    const handleYearChange = (event) => {
        setYear(event.target.value);
    };

    const handleModelChange = (event) => {
        setModel(event.target.value);
    };





    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create an Automobile</h1>
                <form onSubmit={handleSubmit} id="create-automobile-form">

                <div className="form-floating mb-3">
                    <input onChange={handleVinChange} value={vin} placeholder="Automobile Vin" required name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">VIN</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required name="color" id="color" className="form-control"/>
                    <label htmlFor="name">Color</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleYearChange} value={year} placeholder="Year" required name="year" id="year" className="form-control"/>
                    <label htmlFor="name">Year</label>
                </div>

                <div className="mb-3">
                    <select onChange={handleModelChange} value={model_id} required name="model" id="model" className="form-select">
                        <option value="">Choose a Model</option>
                        {models.map((model) => {
                            return (
                                <option key={model.id} value={model.id}>
                                    {model.manufacturer.name} {model.name}
                                </option>
                            );
                        })}
                    </select>
                </div>

                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
      </div>
    );
}
