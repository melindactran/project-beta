import React, { useEffect, useState } from 'react';

export default function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        const data = await response.json();
        setManufacturers(data.manufacturers)
    }
    useEffect(() => {
        getData()
    }, [])
    return (
        <div>
            <h1>List of Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturers => {
                        return (
                            <tr key={manufacturers.id}>
                                <td>{manufacturers.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
