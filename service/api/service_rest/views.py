from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Technician, AutomobileVO, Appointment
import json


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "vip",
        "id",
    ]
    encoders = {"technician": TechnicianEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )

    else:  # POST
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician not created"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"},
            )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        vehicles = AutomobileVO.objects.all()
        vehicle_list = [vehicle.vin for vehicle in vehicles]
        vip_vehicle = []
        for appointment in appointments:
            if appointment.vin in vehicle_list:
                appointment.vip = True
            else:
                appointment.vip = False
            vip_vehicle.append(appointment)

        return JsonResponse(
            {"appointments": vip_vehicle},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"},
            )
            response.status_code = 404
            return response
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"},
            )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = Appointment.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=AppointmentEncoder,
                safe=False,
                )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"},
            )
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_status_canceled(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.status = "canceled"
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)


@require_http_methods(["PUT"])
def api_status_finished(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
