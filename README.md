# CarCar

Team:

    Melinda Tran - Service

    Angel Lawler Winn - Sales

## How to Run this App

1. From GitLab, fork and clone the repository onto your local computer

2. In the project-beta directory, run the following commands in the terminal:

"

    docker volume create beta-data
    docker-compose build
    docker-compose up

"

3. Check that all the containers are running properly on Docker

4. Navigate to http://localhost:3000/ to see the front end


## Diagram

![Project-Beta-Diagram](images/Project-Beta-Diagram.png)

- Link to diagram on Excalidraw
https://excalidraw.com/#json=ZfI8UqztsR1bxCGKJ0FZy,nmBIuHLZKGUX3xLeXEcTNQ

## API Documentation


## Service Microservice

In the Services domain, we have three important backend models: Technician, Appointment, and Automobile Value Object. Technicians have names and employee numbers, Appointments hold information such as vehicle VIN, customer details, date and time, service status, and VIP status. The VIP status is determined using the Automobile Value Object retrieved from the Inventory microservice. Users can create, cancel, complete appointments, assign Technicians, and search for a car's service history by VIN.



## Sales Microservice
In our sales microservice, we have four models: AutomobileVO, Customer, SalesPerson, and SalesRecord. The SalesRecord model interacts with the other three models to retrieve data. The AutomobileVO serves as a value object, gathering information about the inventory through a poller. This poller automatically keeps our sales microservice updated with the latest data. The reason for integrating with the inventory microservice is simple: when recording a new sale, we need to select the specific car being sold, and that information resides in the inventory microservice. This integration ensures a seamless sales process.

## URLs and Ports
- **Service microservice:**
    - http://localhost:8080/

        List technicians - GET -
        http://localhost:8080/api/technicians/

            {
                "technicians": [
                    {
                        "first_name": "Melinda",
                        "last_name": "Tran",
                        "employee_id": 1,
                        "id": 1
                    }]
            }


        Create a technician	- POST -
        http://localhost:8080/api/technicians/

                    {
                        "first_name": "Melinda",
                        "last_name": "Tran",
                        "employee_id": 1,
                        "id": 1
                    }

        Delete a specific technician - DELETE -
        http://localhost:8080/api/technicians/:id

            {
                "deleted": true
            }

        List appointments - GET	-
        http://localhost:8080/api/appointments/

            {
                "appointments": [
                    {
                        "href": "/api/appointments/1/",
                        "date_time": "2023-06-09 17:57",
                        "reason": "just because",
                        "status": "finished",
                        "vin": "1234",
                        "customer": "Melinda",
                        "technician": {
                            "href": "/api/technicians/1/",
                            "first_name": "Jimmy",
                            "last_name": "Boyyy",
                            "employee_id": "4321",
                            "id": 1
                        },
                        "id": 1,
                        "vip": false
                    }
                ]
            }

        Create an appointment - POST -
        http://localhost:8080/api/appointments/

            {
                "date_time": "2023-06-09",
                "reason": "just because",
                "status": "VIP",
                "vin": "1234",
                "customer": "Melinda",
                "technician": "1"
            }

        Delete an appointment - DELETE -
        http://localhost:8080/api/appointments/:id

            {
                "deleted": true
            }

        Set appointment status to "canceled" - PUT -
        http://localhost:8080/api/appointments/:id/cancel

            {
                "href": "/api/appointments/1/",
                "date_time": "2023-06-09 09:00",
                "reason": "just because",
                "status": "canceled",
                "vin": "54321",
                "customer": "Mel T",
                "technician": {
                    "href": "/api/technicians/1/",
                    "first_name": "John",
                    "last_name": "Smith",
                    "employee_id": "321",
                    "id": 1
                },
                "id": 1,
                "vip": false
            }

        Set appointment status to "finished" - PUT -
        http://localhost:8080/api/appointments/:id/finish


            {
                "href": "/api/appointments/1/",
                "date_time": "2023-06-09 09:00",
                "reason": "just because",
                "status": "finished",
                "vin": "54321",
                "customer": "Mel T",
                "technician": {
                    "href": "/api/technicians/1/",
                    "first_name": "John",
                    "last_name": "Smith",
                    "employee_id": "321",
                    "id": 1
                },
                "id": 1,
                "vip": false
            }

- **Sales microservice:**
    - http://localhost:8090/

        List salespeople - GET -
        http://localhost:8090/api/salespeople/

            {
                "salespeople": [
                    {
                        "first_name": "Angel",
                        "last_name": "Lawler Winn",
                        "employee_id": 1,
                        "id": 1
                    }]
            }

        Create a salesperson - POST -
        http://localhost:8090/api/salespeople/


                    {
                        "first_name": "Angel",
                        "last_name": "Lawler Winn",
                        "employee_id": 1,
                        "id": 1
                    }


        Delete a specific salesperson - DELETE -
        http://localhost:8090/api/salespeople/:id

            {
                "deleted": true
            }

        List customers - GET -
        http://localhost:8090/api/customers/

            {
                "customers": [
                    {
                        "name": "Melinda Tran",
                        "address": "123 Main Street",
                        "phone_number": "5555555"
                    },
                    {
                        "name": "Angel Lawler Winn",
                        "address": "1321 Main Street",
                        "phone_number": "5555555"
                    }
                ]
            }

        Create a customer - POST -
        http://localhost:8090/api/customers/

            {
                "name": "Melinda Tran",
                "address": "123 Main Street",
                "phone_number": "5555555"
            }

        Delete a specific customer - DELETE -
        http://localhost:8090/api/customers/:id

            {
                "deleted": true
            }

        List sales - GET -
        http://localhost:8090/api/sales/

            {
                "sales": [
                    {
                        "id": 1,
                        "price": 1000000,
                        "vin": {
                            "vin": "321"
                        },
                        "salesperson": {
                            "id": 1,
                            "name": "Mel T",
                            "employee_number": 1
                        },
                        "customer": {
                            "name": "Angel",
                            "address": "321 Main Street",
                            "phone_number": "5555555"
                        }
                    }
                ]
            }

        Create a sale - POST -
        http://localhost:8090/api/sales/

            {
                "salesperson": "Mel T",
                "customer": "Angel L W",
                "vin": "321",
                "price": 10000000
            }

        Delete a sale - DELETE -
        http://localhost:8090/api/sales/:id

            {
                "deleted": true
            }


- **Inventory microservice:**
    - http://localhost:8100/

    - Manufacturers:

        List manufacturers - GET -
        http://localhost:8100/api/manufacturers/

        Create a manufacturer - POST -
        http://localhost:8100/api/manufacturers/

        Get a specific manufacturer - GET -
        http://localhost:8100/api/manufacturers/:id/

        Update a specific manufacturer - PUT -
        http://localhost:8100/api/manufacturers/:id/

        Delete a specific manufacturer - DELETE	-
        http://localhost:8100/api/manufacturers/:id/

    - Models:

        List vehicle models - GET -
        http://localhost:8100/api/models/

        Create a vehicle model - POST -
        http://localhost:8100/api/models/

        Get a specific vehicle model - GET -
        http://localhost:8100/api/models/:id/

        Update a specific vehicle model - PUT -
        http://localhost:8100/api/models/:id/

        Delete a specific vehicle model - DELETE -
        http://localhost:8100/api/models/:id/

    - Automobiles:

        List automobiles - GET -
        http://localhost:8100/api/automobiles/

        Create an automobile - POST	-
        http://localhost:8100/api/automobiles/

        Get a specific automobile - GET -
        http://localhost:8100/api/automobiles/:vin/

        Update a specific automobile - PUT -
        http://localhost:8100/api/automobiles/:vin/

        Delete a specific automobile - DELETE -
        http://localhost:8100/api/automobiles/:vin/
